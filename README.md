# OIDC client stubs for AURA

In this repository we provide different client stubs and functions, which
can be used to authenticate against AURA components and use the resulting
bearer token to access the different AURA APIs.

If you are unsure what all this OIDC is about, take a look at Nate Barbettini's
talk [OAuth 2.0 and OpenID Connect (in plain English)](https://www.youtube.com/watch?v=996OiexHze0).
You can also take a peek at the [OpenID Connect FAQs and Q&As](https://openid.net/connect/faq/)
if want to check if you already know everything you need to know about the topic.

## Setup of the OIDC client

In any case you will need to have a correct setup of an OIDC client in the
AURA Steering component. The values you configure there have to correspond
with the values you set in your client config file, whether you use the
Shell, Python or Javascript functions.

Also make sure to have the correct response type set, that corresponds to
the OIDC flow you use in your client.

In the next section specifc setups of the client stubs are explained.

## Usage

### Bash

For the shell functions take a look at [bash/main.sh](bash/main.sh) as a
starting point. This shows how to use the include files and get a token
with one of the three provided flows.

### Python

The file [python/main.py](python/main.py) provides a full demo of all flows
that are implemented.

You can use it directly to retrieve tokens and make a test call to the steering
and tank APIs with a token. For that to work, you have to setup your config
first:

- change into the _python_ directory
- copy the _sample.config.py_ file to _config.py_ and update any fields in it
  that do not yet correspond to your OIDC setup and the user account you want
  to use for this
- now call the _main.py_ script with the _-h_ option to get some usage info

```bash
$ python3 main.py -h
```

Now you are good to go and facilitate the demo client to retrieve access codes
and tokens. If you are only interested in using the relevant functions from
the steering package, take a look at the few lines in the conditional blocks
in _main.py_ that start with a comment like `# demo of the ... flow`. This
is all you need, when you want to integrate this into your own script.
Make sure to have the config right, that can be imported from _config.py_
(or can also be set directly in your script) and to set the `code` value
in the `parameters` dictionary, depending on what flow you choose (e.g.
`parameters["code"] = "it_token token"` for an implicit flow).

#### Usage in other Python projects

The Python part of this project has been used in an API-based database migration
tool for AURA at Radio Orange, Vienna. Therefore some fixes have been made and
functionality has been added. Before installation the oidc client was tested by
running the `main.py` test script which revealed a number of bugs which made it 
necessary to amend flow_stages.py and the config file - see section [fixes](#fixes)

For Python projects it is recommended to rather copy the client's project files
over to the other project than trying to include them e.g. as submodule. Ideally
create a directory somewhere where your own files can see the client's files
and copy over the contents of the `python` directory. You will have to create a
client in steering or on the commandline. Copy `sample.config.py` to `config.py`
and edit according to your steering client.

By importing `authorization` from `steering` you get a handful of useful methods
that allow you to automate authorization respectively to make it implicit in
API calls.

An example authorization handled in a class `BaseRestConnector`:

```python
import json
from oidc.config import config as oidc_config
from oidc.steering import authorization
from oidc.tank import session

class BaseRESTConnector():
    """
    Do OIDC authorization before instancing any RESTConnectors.

    Allows us to make OIDC authentication implicit in all RESTConnectors.
    """
    oidc_config["verbosity"] = 0
    params = { "response_type": "code" }
    oidc = authorization.get_auth_code(oidc_config, params)
    oidc = authorization.get_token_from_code(oidc_config, oidc["code"])
    # print("Bearer " + oidc["access_token"])
    tank_session = session.get_session_token(oidc_config, oidc["access_token"]).text
    tank_token = json.loads(tank_session)["token"]
```

Later in your requests, if you receive a `401`:

```python
BaseRESTConnector.oidc = authorization.refresh_access_token(oidc_config, BaseRESTConnector.oidc["refresh_token"])
```

The same thing may occur when communicating with tank as we established the
connection via steering oidc client. However, `401` can also mean the (tank)
session became invalid in which case you should be able to recreate it like
this:

```python
tank_session_request = session.get_session_token(oidc_config, BaseRESTConnector.oidc["access_token"])
if tank_session_request.status_code == 200:
    BaseRESTConnector.tank_token = json.loads(tank_session_request.text)["token"]
```

`get_session_token()` implements a full session creation in tank, using `oidc`
as backend (`tank/session.py`).

##### Fixes:
- Python config: add parameter `host_url` which is basically `base_url` without 
the `/steering` sufix.
- The authorisation process has to operate on html forms which are filtered by
regex patterns. Attribute values all seem to use double quotes for their values.
Regex patterns have been adapted accordingly.
- Python doesn't seem to like URL-encoded strings. URLs are now being prepared
before they are being processed.
- Use raise instead of sys.exit in error cases (not absolutely sure this has
been done correctly)

##### added:

- `refresh_access_token()` (in `authorization`)

### Javascript

For the JS client the approach is a bit different, as the most probable
usage will be on some webspace and therefore we can use the browser for
all redirecting. While the implementation of the flow stages is therefore
much simpler, we need a separate callback page.

Also if you test this locally as a _file://_ in the browser, the automatic
redirect to the callback page after successful authentication will not work.
In that case you have to use the developer console and check the last POST
request in the network monitor and use the `Location:` header manually.
If you put this on any local or remote web server so that you can access it
through HTTP, this should work all fine.

Take a look at the [javascript/index.html](javascript/index.html) page for
a start. The [javascript/steering/implicit.js](javascript/steering/implicit.js)
and the [javascript/steering/flow_stages.js](javascript/steering/flow_stages.js)
are quite short, compared to their Python and Bash counterparts, because we
let the browser and the user handle all forms and redirects. The callback
is then handled by the [javascript/callback.html](javascript/callback.html)
page, which returns to _index.html_ after it has written all relevant info
into the browsers localStorage, so that the index page can access and display
it.

For everything to work fine, first create a copy of the _config.sample.js_
file to _config.js_ and update the fields that do not correspond with your
specific setup. Make sure to use appropriate redirect URIs in the config
as well as in the OIDC client setup in _steering_.

> __Note on structuring__: this could all certainly be more compact in one file
> rather then splitting it to _main.js_, _implicit.js_ and _flow_stages.js_.
> Even the separate _callback.html_ would not be neccessary if we let
> _index.html_ also handle the callback. But this way the code can be
> better compared to the Python and Bash client stubs.

## Planned and upcoming features

- Functions to refresh token before it expires
- Add at least one call to the API including a bearer token in the main demos
    - done for python, still todo for bash and javascript
- Test (and provide sensible error message) for
    - invalid flow type
    - invalid redirect uris
    - invalid credentials
- For the bash client stub:
    - add user-agent header and make it configurable
    - make functions exit at the end of each stage in case of errors
    - make it fully POSIX/dash compliant
- For the python client stub:
    - use raise instead of sys.exit in error cases
- Add checks for state and nonce
- Check, why hybrid flow does not return access code
