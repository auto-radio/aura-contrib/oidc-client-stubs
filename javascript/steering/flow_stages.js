const flow_stages = {
  initiate_flow (cfg, parameters) {
    let url = cfg.base_url + cfg.authorize_endpoint + "?" +
      "client_id=" + cfg.client_id + "&" +
      "scope=" + cfg.scope + "&" +
      "redirect_uri=" + cfg.redirect_uri + "&" +
      "response_type=" + parameters.response_type + "&" +
      "state=" + parameters.state + "&" +
      "nonce=" + parameters.nonce + "&"
    if (cfg.verbosity) {
      console.log("Redirecting to", url)
    }
    location.assign(url)
  },
}
