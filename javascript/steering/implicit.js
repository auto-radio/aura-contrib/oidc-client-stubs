
const implicit = {
  get_token (cfg, parameters) {
    parameters.state = btoa(Math.random())
    parameters.nonce = btoa(Math.random())
    if (cfg.verbosity) {
      console.log("Parameters used to initiate OIDC flow:", parameters)
    }
    parameters.location = flow_stages.initiate_flow(cfg, parameters)
  },
}
