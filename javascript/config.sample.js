let cfg = {
  // The user name which should be used to authenticate
  username: "janedoe",
  // The users password
  password: "choose_a_secure_password_here",

  // What scopes you want to access. Has to contain at least openid, for
  // tank to work you also need aura_shows
  scope: "openid profile email username aura_shows",

  // The base URL of AURA steering
  base_url: "http://localhost:8000",

  // The OIDC authorize, userinfo and token endpoints
  authorize_endpoint: "/openid/authorize",
  userinfo_endpoint: "/openid/userinfo",
  token_endpoint: "/openid/token",

  // The base URL of AURA tank
  tank_base: "http://localhost:8040",
  // The session endpoint at tank
  tank_session_endpoint: "/auth/session",

  // ID of the OIDC client that is configured in AURA Steering
  client_id: "1234567",
  // If an authorization code flow is choosen, a client secret will be needed
  client_secret: "abcdef123456789abcdef123456789abcdef123456789abcdef12345",

  // The redirect URI that is configured in AURA steering for this client
  redirect_uri: "http://localhost:8080/oidc_callback.html",
}
