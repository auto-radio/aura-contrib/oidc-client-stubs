
// some of the UI elements we need for user interaction
const checkboxDebug = document.getElementById("checkboxDebug")
const divNotLoggedIn = document.getElementById("notLoggedIn")
const divAuthCode = document.getElementById("authCode")
const divLoggedIn = document.getElementById("loggedIn")
const spanAccessCode = document.getElementById("access_code")
const spanAccessToken = document.getElementById("access_token")
const spanIDToken = document.getElementById("id_token")
const spanUserID = document.getElementById("user_id")
const spanUserEmail = document.getElementById("user_email")
const spanTokenExpires = document.getElementById("token_expires")
const spanTokenRetrieved = document.getElementById("token_retrieved")
const spanExpiredNotice = document.getElementById("expired_notice")

// when the debug output checkbox is clicked
checkboxDebug.addEventListener("click", function () {
  if (checkboxDebug.checked) {
    cfg.verbosity = 1
    localStorage.oidcCallbackDebugging = true
    console.log("Verbose output activated.")
  } else {
    cfg.verbosity = 0
    localStorage.removeItem("oidcCallbackDebugging")
    console.log("Verbose output deactivated. I'll be silent now.")
  }
})

// check the debug output checkbox, if it was checked the last time
if (localStorage.oidcCallbackDebugging) {
  checkboxDebug.checked = true
  cfg.verbosity = 1
}

// check the localStorage if we already have a user object with a valid token
let userObject
if (localStorage.user === undefined) {
  divNotLoggedIn.style.display = "block"
} else {
  userObject = JSON.parse(localStorage.user)
  if (cfg.verbosity) {
    console.log('Current user object:', userObject)
  }
  // authorization code flow only returns an access code
  if (userObject.code) {
    divAuthCode.style.display = "block"
    spanAccessCode.innerText = userObject.code
  } else {
    divLoggedIn.style.display = "block"
    spanAccessToken.innerText = userObject.access_token
    spanIDToken.innerText = userObject.id_token
    spanUserID.innerText = userObject.id_token_contents.sub
    spanUserEmail.innerText = userObject.id_token_contents.email
    spanTokenRetrieved.innerText = String(new Date(userObject.id_token_contents.iat*1000))
    spanTokenExpires.innerText = String(new Date(userObject.id_token_contents.exp*1000))
    if (new Date(userObject.id_token_contents.exp*1000) < new Date()) {
      spanExpiredNotice.style.display = "inline"
    } else {
      // if the token has not yet expired, let's check every 5 seconds again
      let expiryChecker = setInterval(function () {
        console.log("checking for expiry")
        if (new Date(userObject.id_token_contents.exp*1000) < new Date()) {
          if (cfg.verbosity) {
            console.log("Token has now expired. Deactivating the expiryChecker")
          }
          spanExpiredNotice.style.display = "inline"
          clearInterval(expiryChecker)
        }
      }, 1000 * 5)
    }
  }
}

// this is how we actually start an implicit flow, when the user hits the button
const start_implicit = function () {
  let parameters = {
    response_type: "id_token token"
  }
  if (cfg.verbosity) {
    console.log("Starting an implicit OIDC flow.")
  }
  implicit.get_token(cfg, parameters)
}
