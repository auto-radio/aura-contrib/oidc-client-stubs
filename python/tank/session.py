import requests

def get_session_token(cfg, access_token):
    """
    Retrieves a session token from tank, by using an access token previously
    received from steering.
    """
    headers = {"User-Agent": cfg["user_agent"]}
    if cfg["verbosity"] >= 1:
        print("[tank]: sending access token to session endpoint")

    url = cfg["tank_base"] + cfg["tank_session_endpoint"]
    headers["Authorization"] = "Bearer " + access_token
    payload = {
        "backend": "oidc",
        "arguments": {
            "access_token": access_token,
            "token_type": "Bearer",
        }
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
    except Exception as e:
        print(f"An Error occurred", e)
        raise e

    if cfg["verbosity"] >= 2:
        print("tank session:", response.json())
    return response
