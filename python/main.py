#!/usr/bin/python3

import argparse
from config import config as cfg
from steering import implicit
from steering import authorization
import tank.session
import requests


parser = argparse.ArgumentParser(
    description="Demo for the OIDC client functions/stubs for AURA.")
parser.add_argument("-v", "--verbosity", action="count",
    help="Activate verbose output. Use several times to increase")
parser.add_argument("-s", "--steering", action="store_true",
    help="Make an authorised test call against the steering API")
parser.add_argument("-t", "--tank", action="store_true",
    help="Make an authorised test call against the tank API")
parser.add_argument("-m", "--mode", choices=["implicit", "authorization"],
    default="implicit", help="The OIDC flow mode (default: %(default)s)")
args = parser.parse_args()

if args.verbosity:
    cfg["verbosity"] = args.verbosity
else:
    cfg["verbosity"] = 0


# we have to set the response_type according to our flow type
parameters = { "response_type": "id_token token" }
if args.mode == "authorization":
    parameters["response_type"] = "code"

# now we can retrieve an access token from steering
if args.mode == "implicit":
    # demo of the implicit flow
    if cfg["verbosity"] >= 1:
        print("Using implicit flow")
    oidc = implicit.get_token(cfg, parameters)
elif args.mode == "authorization":
    # demo of the authorization code flow
    if cfg["verbosity"] >= 1:
        print("Using authorization code flow")
    oidc = authorization.get_auth_code(cfg, parameters)
    oidc = authorization.get_token_from_code(cfg, oidc["code"])

if cfg["verbosity"] == 0:
    print("access token:", oidc["access_token"])

# with the steering access token we can also retrieve a session token from tank
tank_session = tank.session.get_session_token(cfg, oidc["access_token"])
if cfg["verbosity"] == 0:
    print("tank session token:", tank_session["token"])

## this is to demonstrate a steering API call with the retrieved access token
if args.steering:
    print("Accessing steering userinfo endpoint, to retrieve user ID.")
    url = cfg["base_url"] + cfg["userinfo_endpoint"]
    headers = {
        "User-Agent": cfg["user_agent"],
        "Authorization": "Bearer " + oidc["access_token"]
    }
    try:
        response = requests.get(url, headers=headers)
    # except:
    #     e = sys.exc_info()
    #     print(e[0].__name__, ':', e[1])
    #     sys.exit(1)
    except Exception as e:
        print("Error while sending access_token", e)
        raise e

    resp = response.json()
    uid = resp["sub"]
    privileged = resp["privileged"]

    print("Accessing steering shows endpoint.")
    url = cfg["base_url"] + "/api/v1/shows"
    payload = { "owner": uid }
    try:
        response = requests.get(url, headers=headers, params=payload)
    # except:
    #     e = sys.exc_info()
    #     print(e[0].__name__, ':', e[1])
    #     sys.exit(1)
    except Exception as e:
        print("Error while accessing steering endpoint", e)
        raise e

    shows = response.json()
    print("Shows in steering you own:")
    for show in shows:
        print("  [" + str(show["id"]) + "]", show["name"])
    if privileged:
        print("As you are a privileged user you have access to all other shows too.")

## this is to demonstrate a tank API call with the retrieved access token
if args.tank:
    print()
    print("Accessing tank shows endpoint.")
    url = cfg["tank_base"] + "/api/v1/shows"
    headers = {
        "User-Agent": cfg["user_agent"],
        "Authorization": "Bearer " + tank_session["token"]
    }
    try:
        response = requests.get(url, headers=headers)
    except Exception as e:
        print("Error while accessing tank endpoint", e)
        raise e

    shows = response.json()
    print("Shows you have access to on tank, that already have some content:")
    for show in shows["results"]:
        print("  -", show["name"])
