config = {
    # The user name which should be used to authenticate
    "username": "janedoe",
    # The users password
    "password": "choose_a_secure_password_here",

    # What scopes you want to access. Has to contain at least openid, for
    # tank to work you also need aura_shows
    "scope": "openid profile email username aura_shows",

    # Depending on the URL scheme under which AURA has been installed
    # it is useful to define the following two variables separately.
    # If AURA components are accessible through different ports
    # (e.g. http://localhost:8000 for steering and http://localhost:8040
    # tank) host_url and base_url should be identical

    # The host URL - if steering is located in a virtual subdirectory 
    # (e.g. http(s)://aura-host/steering), set this to http(s)://<aura-host>
    "host_url": "http://localhost:8000",

    # The base URL of AURA steering.
    # If steering is accessible e.g. in /steering this parameter should
    # should reflect that location: http(s)://<aura/host>/steering
    "base_url": "http://localhost:8000",

    # The OIDC authorize, userinfo and token endpoints
    "authorize_endpoint": "/openid/authorize",
    "userinfo_endpoint": "/openid/userinfo",
    "token_endpoint": "/openid/token",

    # The base URL of AURA tank, analog to base_url
    "tank_base": "http://localhost:8040",
    # The session endpoint at tank
    "tank_session_endpoint": "/auth/session",

    # ID of the OIDC client that is configured in AURA Steering
    "client_id": "1234567",
    # If an authorization code flow is choosen, a client secret will be needed
    "client_secret": "abcdef123456789abcdef123456789abcdef123456789abcdef12345",

    # The redirect URI that is configured in AURA steering for this client.
    # Though the URL needs to be defined manually the page it is pointing to
    # does not need to exist.
    "redirect_uri": "http://localhost:8080/oidc_callback.html",

    # A string representing your client (optional, but useful for debugging)
    "user_agent": "AURA Python Client Stub 0.1",
}
