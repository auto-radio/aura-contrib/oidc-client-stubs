from random import randint
from . import flow_stages
import requests

def get_auth_code (cfg, parameters):
    # generate a nonce and a random state
    parameters["state"] = ''.join([str(randint(0, 9)) for i in range(16)]),
    parameters["nonce"] = ''.join([str(randint(0, 9)) for i in range(16)]),
    # initiate the OIDC flow and set the location for the login form
    parameters["location"] = flow_stages.initiate_flow(cfg, parameters)
    # submit the login form and retrieve the callback URL
    parameters["callback"] = flow_stages.handle_login_form(cfg, parameters)
    # return the token information extracted from the callback
    return flow_stages.get_token_from_callback(cfg, parameters["callback"])


def get_token_from_code (cfg, code):
    """
    Retrieves bearer and ID tokens from AURA steering, given a correct
    authorization code and client secret are provided
    """
    url = cfg["base_url"] + cfg["token_endpoint"]
    headers = {"User-Agent": cfg["user_agent"]}
    payload = {
        "client_id": cfg["client_id"],
        "client_secret": cfg["client_secret"],
        "code": code,
        "redirect_uri": cfg["redirect_uri"],
        "grant_type": "authorization_code",
    }
    try:
        if cfg["verbosity"] >= 1:
            print("Calling OIDC token endpoint to exchange access code for token")
        response = requests.post(url, headers=headers, data=payload, allow_redirects=False)
    # except Error as e:
    #     e = sys.exc_info()
    #     print(e[0].__name__, ':', e[1])
    #     sys.exit(1)
    except Exception as e:
        print("Error while requesting token from code", e)
        raise e
    if cfg["verbosity"] >= 2:
        print(response.json())
    return response.json()


def refresh_access_token (cfg, refresh_token):
    """
    Refresh the access_code if access_code has expired
    """
    url = cfg["base_url"] + cfg["token_endpoint"]
    headers = {"User-Agent": cfg["user_agent"]}
    payload = {
        "client_id": cfg["client_id"],
        "client_secret": cfg["client_secret"],
        "grant_type": "refresh_token",
        "refresh_token": refresh_token
    }
    try:
        if cfg["verbosity"] >= 1:
            print("Calling OIDC token endpoint to refresh access_token")
        response = requests.post(url, headers=headers, data=payload, allow_redirects=False)
        if response.status_code == 200:
            print("*** New access token created successfully ***")
        else:
            print(f"Something went wrong while requesting new access token: {response.status_code} | {response.text}")
    except Exception as e:
        print("Error while refreshing access_token:", e)
        raise e
    if cfg["verbosity"] >= 2:
        print(response.json())
    return response.json()
