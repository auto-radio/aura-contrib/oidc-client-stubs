import html
import requests
import sys
import re

from urllib.parse import unquote

def initiate_flow (cfg, parameters):
    """
    Initiates an OIDC flow and returns the URL to the login form
    """
    if cfg["verbosity"] >= 1:
        print("## Stage 1: accessing the authorize endpoint")
    headers = {"User-Agent": cfg["user_agent"]}
    payload = {
        "client_id": cfg["client_id"],
        "scope": cfg["scope"],
        "redirect_uri": cfg["redirect_uri"],
        "response_type": parameters["response_type"],
        "state": parameters["state"],
        "nonce": parameters["nonce"],
    }
    url = cfg["base_url"] + cfg["authorize_endpoint"]
    try:
        if cfg["verbosity"] >= 2:
            print("Initiating flow at:", url)
        response = requests.get(url, headers=headers, params=payload, allow_redirects=False)
    except:
        e = sys.exc_info()
        print(e[0].__name__, ':', e[1])
        sys.exit(1)

    # response.headers["Location"] should contain a fully qualified URL
    if cfg["verbosity"] >= 2:
        print("login form URL:", response.headers["Location"])
    return response.headers["Location"]


def handle_login_form (cfg, parameters):
    """
    Requests the login form and submits all necessary data, including a separate
    post to confirm consent if required by the auth backend. After success the
    final callback redirect URL is returned
    """
    if cfg["verbosity"] >= 1:
        print("## Stage 2: accessing the login form")
    headers = {"User-Agent": cfg["user_agent"]}
    url = parameters["location"]
    if cfg["verbosity"] >=2:
        print(f"url: {url}")
    try:
        response = requests.get(url, headers=headers, allow_redirects=False)
    except Exception as e:
        print("Error while requesting the login form", e)
        raise e


    # save cookies (including csrf token and session id), and extract form data
    jar = response.cookies
    m = re.search('<form action="([^"]*)"', response.text)
    submit_url = m.groups()[0]
    # different steering versions seem to be inconsistent in using ' or " for the
    # arguments of the csrfmiddlewaretoken input tag
    m = re.search("<input type=['\"]hidden['\"] name=['\"]csrfmiddlewaretoken['\"] value=['\"]([^'\"]*)['\"]", response.text)
    csrf_mw_token = m.groups()[0]
    m = re.search("<input type=['\"]hidden['\"] name=['\"]next['\"] value=['\"]([^'\"]*)['\"]", response.text)
    next_field = m.groups()[0]

    # submit login data
    # submit_url will already be prependet with "/steering".
    # Hence, we use cfg["host_url"] instead of cfg["base_url"]
    url = cfg["host_url"] + submit_url
    payload = {
        "username": cfg["username"],
        "password": cfg["password"],
        "csrfmiddlewaretoken": csrf_mw_token,
        "next": next_field,
    }
    if cfg["verbosity"] >= 2:
        print("CSRF cookie:", jar.get("csrftoken"))
        print("CSRF middleware token:", csrf_mw_token)
        print("next field:", next_field)
        print("submit URL:", submit_url)
    try:
        if cfg["verbosity"] >= 1:
            print("## Stage 3: submitting login data")
        response = requests.post(url, headers=headers, cookies=jar, data=payload, allow_redirects=False)
    except Exception as e:
        print("Error while submitting login data", e)
        raise e

    if cfg["verbosity"] >= 2:
        print("CSRF cookie:", jar.get("csrftoken"))
        print("session cookie:", jar.get("sessionid"))

    # in case the login was successful, we should receive a 302. if we receive
    # a 200, it means that the credentials have to be entered again because they
    # are not correct
    if (response.status_code == 200):
        print("Username and/or password are not correct!")
        sys.exit(1)

    # attempt to retrieve final callback redirect
    jar = response.cookies
    # python doesn't like URL encoded strings it seems...
    next_field = unquote(next_field)
    next_field = html.unescape(next_field)
    url = cfg["base_url"] + next_field
    try:
        if cfg["verbosity"] >= 1:
            print("## Stage 4: get token in final callback location")
        response = requests.get(url, headers=headers, cookies=jar, allow_redirects=False)
    except Exception as e:
        print("Error while trying to retrieve final callback redirect", e)
        raise e


    # if explicit consent is required, we will not be redirected but get a
    # consent form which we have to submit, before the final redirect can happen
    if response.status_code != 302:
        if cfg["verbosity"] >= 1:
            print("steering requires explicit consent")
        # extract form data (cookies from last request have to be reused)
        m = re.search('<form method="post" action="([^"]*)"', response.text)
        submit_url = m.groups()[0]
        m = re.search('<input type="hidden" name="csrfmiddlewaretoken" value="([^"]*)"', response.text)
        csrf_mw_token = m.groups()[0]
        if cfg["verbosity"] >= 2:
            print("CSRF cookie:", jar.get("csrftoken"))
            print("CSRF middleware token:", csrf_mw_token)
            print("submit URL:", submit_url)

        # submit consent form
        url = cfg["base_url"] + submit_url
        payload = {
            "csrfmiddlewaretoken": csrf_mw_token,
            "client_id": cfg["client_id"],
            "redirect_uri": cfg["redirect_uri"],
            "response_type": parameters["response_type"],
            "scope": cfg["scope"],
            "state": parameters["state"],
            "nonce": parameters["nonce"],
            "allow": "Authorize",
        }
        try:
            if cfg["verbosity"] >= 1:
                print("submitting consent form")
            response = requests.post(url, headers=headers, cookies=jar, data=payload, allow_redirects=False)
        except Exception as e:
            print("Error while submitting consent form", e)
            raise e


    # return callback location
    return response.headers["Location"]


def get_token_from_callback (cfg, url):
    """
    Extract any relevant information from a callback redirect URL
    """
    if cfg["verbosity"] >= 1:
        print("## Stage 5: processing the callback redirect URL")
    if cfg["verbosity"] >= 2:
        print("callback URL:", url)
    oidc = {}
    m = re.search('access_token=([^&]*)', url)
    if m:
        oidc["access_token"] = m.groups()[0]
    m = re.search('id_token=([^&]*)', url)
    if m:
        oidc["id_token"] = m.groups()[0]
    m = re.search('code=([^&]*)', url)
    if m:
        oidc["code"] = m.groups()[0]
    if cfg["verbosity"] >= 1:
        print("Successfully retrieved access codes/tokens:", oidc)
    return oidc
