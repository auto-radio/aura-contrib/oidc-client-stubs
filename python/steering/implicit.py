from random import randint
from . import flow_stages

def get_token (cfg, parameters):
    # generate a nonce and a random state
    parameters["state"] = ''.join([str(randint(0, 9)) for i in range(16)]),
    parameters["nonce"] = ''.join([str(randint(0, 9)) for i in range(16)]),
    # initiate the OIDC flow and set the location for the login form
    parameters["location"] = flow_stages.initiate_flow(cfg, parameters)
    # submit the login form and retrieve the callback URL
    parameters["callback"] = flow_stages.handle_login_form(cfg, parameters)
    # return the token information extracted from the callback
    return flow_stages.get_token_from_callback(cfg, parameters["callback"])
