# get_steering_authorization
#
# Facilitates OIDC Authorization Code Flow to retrieve an access code from
# the AURA steering server
#
# After successfully run, the $CODE variable is set.
get_steering_auth_code () {
  NONCE=$(make_nonce)
  STATE=$(make_nonce)

  # initiate the OIDC flow with the correct response type
  RESPONSE_TYPE="code"
  initiate_flow

  # the other stages will be handled by login form function
  # afterwards we should receive the OUTPUT of the last curl call containing
  # the callback URL with all the required info
  handle_login_form

  if [ $VERBOSITY -ge 1 ]; then
    echo "Stage 4: processing the callback redirect URL"
  fi
  CALLBACK=$(echo -e "${OUTPUT}" | grep '< Location: ' | cut -f 3 -d " ")
  CODE=$(echo "${CALLBACK}" | grep -o "code=.*&state" | \
    cut -f 2 -d "=" | cut -f 1 -d "&"
  )
  if [ $VERBOSITY -ge 2 ]; then
    echo "callback URL: ${CALLBACK}"
  fi

  if [ $VERBOSITY -ge 1 ]; then
    echo "authorization code: ${CODE}"
  fi
}

# get_steering_token_from_code
#
# Retrieves bearer and ID tokens from AURA steering, given a correct
# authorization code and clien secret are provided.
#
# After successfully run, the following variables are set:
#   $TOKEN          the bearer token to access the API
#   $ID_TOKEN       the ID token
#   $REFRESH_TOKEN  a refresh token to retrieve a new bearer token
#   $EXPIRES_IN     time when the current bearer token will expire (in seconds)
get_steering_token_from_code () {
  OUTPUT=$(curl -s -X POST \
    -H "Cache-Control: no-cache" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    "http://localhost:8000/openid/token" \
    -d "client_id=${CLIENT_ID}" \
    -d "client_secret=${CLIENT_SECRET}" \
    -d "code=${CODE}" \
    -d "redirect_uri=${REDIRECT_URI}" \
    -d "grant_type=authorization_code"
  )

  TOKEN=$(echo $OUTPUT | jq .access_token | tr -d '"')
  ID_TOKEN=$(echo $OUTPUT | jq .id_token | tr -d '"')
  REFRESH_TOKEN=$(echo $OUTPUT | jq .refresh_token | tr -d '"')
  EXPIRES_IN=$(echo $OUTPUT | jq .expires_in)

  if [ $VERBOSITY -ge 1 ]; then
    echo "bearer token: ${TOKEN}"
    echo "id token: ${ID_TOKEN}"
    echo "refresh token: ${REFRESH_TOKEN}"
    echo "expires in: ${EXPIRES_IN}"
  fi
}
