# intiate_flow
#
# Initiates and OIDC flow, given the correct RESPONSE_TYPE, which has to
# be set before calling this function to one of the following three:
#   code
#   id_token%20token
#   code%20id_token%20token
# This has to correspond with the Response Type that is set in the Django
# configuration for the OIDC client with the given CLIENT_ID
#
# On success this function returns a URL to the login form of AURA Steering,
# in order to authenticate and provide consent if needed. This is returned by
# setting the $LOGIN_FORM variable.
initiate_flow () {
  if [ $VERBOSITY -ge 1 ]; then
    echo "## Stage 1: accessing the authorize endpoint"
  fi
  INIT_URL="${BASE_URL}${AUTHORIZE_ENDPOINT}?client_id=${CLIENT_ID}"
  INIT_URL+="&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}"
  INIT_URL+="&scope=${SCOPE// /+}&state=${STATE}&nonce=${NONCE}"
  if [ $VERBOSITY -ge 2 ]; then
    echo "Initiating flow at: ${INIT_URL}"
  fi
  OUTPUT=$(curl -s -v "${INIT_URL}" 2>&1)
  LOGIN_FORM=$(echo -e "${OUTPUT}" | grep "< Location:" | cut -f 3 -d " " | \
  tr -d '\r')
  if [ $VERBOSITY -ge 2 ]; then
    echo "login form URL: ${LOGIN_FORM}"
  fi
}
