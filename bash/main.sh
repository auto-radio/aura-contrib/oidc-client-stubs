#!/bin/bash

# Include config and oidc functions
. ./config.sh
. ./oidc.inc.sh

VERBOSITY=0
GETOPTS_ERROR=false

while getopts ":vh" opt; do
  case $opt in
    v)
      VERBOSITY=$((VERBOSITY+1))
      ;;
    h)
      echo "Usage: $0 [-d]"
      echo
      echo "  -v  Activate verbose output. Use several times to increase"\
        "verbosity level."
      echo "  -h  Show this description"
      exit 0
      ;;
    \?)
      echo "Invalid option: -${OPTARG}" >&2
      GETOPTS_ERROR=true
      ;;
  esac
done
if [ "$GETOPTS_ERROR" = true ]; then
  echo "Try -h for help"
  exit 1
fi

if [ $VERBOSITY -ge 1 ]; then
  echo "Applying verbositiy level: ${VERBOSITY}"
fi

## Use the following lines for an implicit flow
get_steering_implicit
echo "bearer token: ${TOKEN}"
echo "id token: ${ID_TOKEN}"

## Use the following lines for a hybrid flow
#get_steering_hybrid

## Use the following lines for an authorization code flow
#get_steering_auth_code
#echo "auth code: ${CODE}"
#get_steering_token_from_code
#echo "bearer token: ${TOKEN}"
#echo "id token: ${ID_TOKEN}"
#echo "refresh token: ${REFRESH_TOKEN}"
#echo "expires in: ${EXPIRES_IN}"
