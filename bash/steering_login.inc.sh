# handle_login_form
#
# After an OIDC flow was initiated this function handles the login process
# at the IDP (AURA Steering).
#
# After success this function returns output of the last curl call containing
# the callback URL with all the required info in the $OUTPUT variable
handle_login_form () {
  if [ $VERBOSITY -ge 1 ]; then
    echo "## Stage 2: accessing the login form"
  fi
  OUTPUT=$(curl -s -v "${BASE_URL}${LOGIN_FORM}" 2>&1)
  CSRF_TOKEN=$(echo -e "${OUTPUT}" | grep "< Set-Cookie:" | cut -f 4 -d " " | \
    tr -d ';'
  )
  FORM_TAG=$(echo -e "${OUTPUT}" | grep "<form action=")
  SUBMIT_URL=$(echo ${FORM_TAG} | grep -o 'action=".*"' | cut -f 2 -d '"')
  CSRF_MW_TOKEN=$(echo ${FORM_TAG} | \
    grep -o "name='csrfmiddlewaretoken' value='.*'" | cut -f 4 -d "'")
  NEXT_FIELD=$(echo -e "${OUTPUT}" | grep '<input type="hidden" name="next" ' | \
    grep -o 'value=".*"' | cut -f 2 -d '"'
  )
  NEXT_FIELD=$(echo ${NEXT_FIELD//&amp;/&})
  if [ $VERBOSITY -ge 2 ]; then
    echo "CSRF cookie: ${CSRF_TOKEN}"
    echo "CSRF middleware token: ${CSRF_MW_TOKEN}"
    echo "next field: ${NEXT_FIELD}"
    echo "submit URL: ${SUBMIT_URL}"
  fi

  if [ $VERBOSITY -ge 1 ]; then
    echo "## Stage 3: submitting login data"
  fi
  OUTPUT=$(curl -s -v --cookie ${CSRF_TOKEN} \
    -F "username=${USERNAME}" -F "password=${PASSWORD}" \
    -F "csrfmiddlewaretoken=${CSRF_MW_TOKEN}" -F "next=${NEXT_FIELD}" \
    "${BASE_URL}${SUBMIT_URL}" 2>&1
  )
  CSRF_TOKEN=$(echo -e "${OUTPUT}" | grep "< Set-Cookie:" | \
    grep -o "csrftoken=.*;" | cut -f 1 -d ';'
  )
  SESSION_ID=$(echo -e "${OUTPUT}" | grep "< Set-Cookie:" | \
    grep -o "sessionid=.*;" | cut -f 1 -d ';'
  )
  if [ $VERBOSITY -ge 2 ]; then
    echo "CSRF cookie: ${CSRF_TOKEN}"
    echo "session cookie: ${SESSION_ID}"
  fi

  if [ $VERBOSITY -ge 1 ]; then
    echo "## Stage 4: get token in final callback location"
  fi
  OUTPUT=$(curl -s -v --cookie "${CSRF_TOKEN}; ${SESSION_ID}" \
    --referer "${BASE_URL}${SUBMIT_URL}" \
    "${BASE_URL}${NEXT_FIELD}" 2>&1
  )

  # check if consent is required
  echo -e "${OUTPUT}" | grep -q '<h1>Request for Permission</h1>'
  if [ $? -eq 0 ]; then
    # in this case we have to extract submit the consent form before we
    # receive the final callback redirect
    if [ $VERBOSITY -ge 1 ]; then
      echo "steering requires explicit consent"
    fi
    CSRF_TOKEN=$(echo -e "${OUTPUT}" | grep "< Set-Cookie:" | \
      grep -o "csrftoken=.*;" | cut -f 1 -d ';'
    )
    CSRF_MW_TOKEN=$(echo -e "${OUTPUT}" | \
      grep -o "name='csrfmiddlewaretoken' value='.*'" | cut -f 4 -d "'"
    )
    SUBMIT_URL=$(echo -e "${OUTPUT}" | \
      grep -o '<form method="post" action=".*">' | cut -f 4 -d '"'
    )
    if [ $VERBOSITY -ge 2 ]; then
      echo "CSRF cookie: ${CSRF_TOKEN}"
      echo "CSRF middleware token: ${CSRF_MW_TOKEN}"
      echo "submit URL: ${SUBMIT_URL}"
    fi
    if [ $VERBOSITY -ge 1 ]; then
      echo "submitting consent form"
    fi
    OUTPUT=$(curl -s -v --cookie "${CSRF_TOKEN}; ${SESSION_ID}" \
      -F "csrfmiddlewaretoken=${CSRF_MW_TOKEN}" \
      -F "client_id=${CLIENT_ID}" \
      -F "redirect_uri=${REDIRECT_URI}" \
      -F "response_type=id_token token" \
      -F "scope=${SCOPE}" \
      -F "state=${STATE}" \
      -F "nonce=${NONCE}" \
      -F "allow=Authorize" \
      "${BASE_URL}${SUBMIT_URL}" 2>&1
    )
    # now we received the final callback redirect and can continue
    # as if no consent would have been required explicitly
  fi
}
