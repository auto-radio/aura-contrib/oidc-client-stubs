. ./steering_login.inc.sh
. ./steering_initiate.inc.sh
. ./steering_authorization.inc.sh
. ./steering_implicit.inc.sh
. ./steering_hybrid.inc.sh

make_nonce () {
  base64 /dev/urandom | tr -d '/+[A-Z][g-z]' | dd bs=32 count=1 status=none | \
    tr -d '\n'
}
