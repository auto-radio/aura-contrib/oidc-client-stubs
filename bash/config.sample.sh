# The user name which should be used to authenticate
USERNAME="janedoe"

# The users password
PASSWORD="choose_a_secure_password_here"

# What scopes you want to access (should at least contain openid)
SCOPE="openid profile email"

# The base URL of AURA steering
BASE_URL="http://localhost:8000"

# The OIDC authorize endpoint
AUTHORIZE_ENDPOINT="/openid/authorize"

# ID of the OIDC client that is configured in AURA Steering
CLIENT_ID="123456"

# If an authorization code flow is choosen, a client secret will be needed
CLIENT_SECRET="abcdef123456789abcdef123456789abcdef123456789abcdef12345"

# The redirect URI that is configured in AURA steering for this client
REDIRECT_URI="http://localhost:8080/oidc_callback.html"
