# get_steering_hybrid
#
# Facilitates OIDC Hybrid Flow to retrieve an authorization code as well as a
# bearer token and an ID token from the AURA steering server.
#
# After successfully run, the $CODE, $TOKEN and $ID_TOKEN variables are set.
get_steering_hybrid () {
  NONCE=$(make_nonce)
  STATE=$(make_nonce)

  # initiate the OIDC flow with the correct response type
  RESPONSE_TYPE="code%20id_token%20token"
  initiate_flow

  # the other stages will be handled by login form function
  # afterwards we should receive the OUTPUT of the last curl call containing
  # the callback URL with all the required info
  handle_login_form

  if [ $VERBOSITY -ge 1 ]; then
    echo "Stage 4: processing the callback redirect URL"
  fi
  CALLBACK=$(echo -e "${OUTPUT}" | grep '< Location: ' | cut -f 3 -d " ")
  TOKEN=$(echo "${CALLBACK}" | grep -o "access_token=.*&id_token" | \
    cut -f 2 -d "=" | cut -f 1 -d "&"
  )
  ID_TOKEN=$(echo "${CALLBACK}" | grep -o "id_token=.*&token_type" | \
    cut -f 2 -d "=" | cut -f 1 -d "&"
  )
  if [ $VERBOSITY -ge 2 ]; then
    echo "callback URL: ${CALLBACK}"
  fi

  if [ $VERBOSITY -ge 1 ]; then
    echo "bearer token: ${TOKEN}"
    echo "id token: ${ID_TOKEN}"
  fi

  echo "TODO:"
  echo "  so far steering does not provide an authorization code"
  echo "  check, whether this is an issue with django-oidc-provider or"
  echo "  if we are missing something here"
  echo "  callback url:"
  echo "  ${CALLBACK}"
}
