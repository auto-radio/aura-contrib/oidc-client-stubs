# get_steering_implicit
#
# Facilitates OIDC Implicit Flow to retrieve a bearer token and an ID token
# from the AURA steering server.
#
# After successfully run, the $TOKEN and $ID_TOKEN variables are set.
get_steering_implicit () {
  NONCE=$(make_nonce)
  STATE=$(make_nonce)

  # initiate the OIDC flow with the correct response type
  RESPONSE_TYPE="id_token%20token"
  initiate_flow

  # the other stages will be handled by login form function
  # afterwards we should receive the OUTPUT of the last curl call containing
  # the callback URL with all the required info
  handle_login_form

  if [ $VERBOSITY -ge 1 ]; then
    echo "Stage 5: processing the callback redirect URL"
  fi
  CALLBACK=$(echo -e "${OUTPUT}" | grep '< Location: ' | cut -f 3 -d " ")
  TOKEN=$(echo "${CALLBACK}" | grep -o "access_token=.*&id_token" | \
    cut -f 2 -d "=" | cut -f 1 -d "&"
  )
  ID_TOKEN=$(echo "${CALLBACK}" | grep -o "id_token=.*&token_type" | \
    cut -f 2 -d "=" | cut -f 1 -d "&"
  )
  if [ $VERBOSITY -ge 2 ]; then
    echo "callback URL: ${CALLBACK}"
  fi

  if [ $VERBOSITY -ge 1 ]; then
    echo "bearer token: ${TOKEN}"
    echo "id token: ${ID_TOKEN}"
  fi
}
